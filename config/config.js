var config = {
	endpoint: '',
	allowedInterpreters: { 'js': 'node', 'rb': 'ruby', 'sh': 'sh', 'py': 'python' },
	db: {
		mongodb: 'localhost:27017/xful'
	},
  response: {
    timeout: 5000, // [ms], checks for data input and exit process after no data is received in this period, is ignored on ws
  },
	host: 'http://localhost',
	port: null,
  attachDebugDataOnResponse: true,
  websockets: {
    active: true,
    port: 34567,
    host: 'wss://localhost',
  },
  baseUrl: function() {
    return config.host.replace(/\/+$/,'')+':'+config.port+'/';
  }
};

module.exports = exports = config;