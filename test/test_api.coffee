require('source-map-support').install()
request = require('request')
sudo = require('sudo')

config =
  port: 5678
  user: 'vagrant'
  password: 'password'
  url: 'http://localhost'

spawn = require('child_process').spawn
app = null

apiCall = (method, url, data, cb) ->
  uri = config.url+':'+config.port+'/'+url.replace(/^\/+/,'')
  if typeof data is 'function'
    cb = data
    data = {}
  # console.log(uri)
  request { method: method.toUpperCase(), url: uri, json: true, body: data }, cb

describe 'api', ->

  before (done) ->
    app = sudo([ 'PORT='+config.port, 'USER='+config.user, '/usr/bin/node', 'app.js' ])

    # setTimeout ->
    #   done()
    # , 2000

    hasStarted = false

    app.stderr.on 'data', (data) ->
      # console.error String(data)

    app.stdout.on 'data', (data) ->
      unless hasStarted
        data = String(data)
        if String(data).match('listening on port '+config.port)
          hasStarted = true
          done()

    
      

  after (done) ->
    
    app.on 'close', (err) ->
      console.log "server closed (port #{config.port})"
    app.kill('SIGHUP')  
    done()

  describe 'auth', ->

    it 'expect to do sth', (done) ->
      done()

  describe 'user', ->

    it 'expect to add a user', (done) ->
      apiCall 'get', '/user', (err, res) ->
        console.log res.body.data
        done()

