var express = require('express');
var routes = require('./routes/routes');
var path = require('path');
var config = require('./config');
var helpers = require('./app/helpers');

var app = express();
var server = require('http').createServer(app);


// all environments
app.set('port', process.env.PORT || 3456);
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.use(routes.index);

config.port = app.get('port');

server.listen(app.get('port'), function(){
  helpers.log('**important** WebServer is available on ' + config.baseUrl());
});



if (config.websockets.active) {

  var WebSocketServer = require('ws').Server;
  var wss = new WebSocketServer({port: config.websockets.port});
  var wsUrl = config.websockets.host || config.host.replace(/^http(s)*\:\/\//,'wss://');
  wss.on('connection', function(ws) {

    ws.on('close', function() {

    });

    ws.on('message', function(data, flags) {
      var segments = data.match(/^([A-Z]+)\:(.+?)(\?(.*))*$/);
      var query = segments[4];
      var parameters = {};
      if (query) {
        query.split('&').forEach(function(queryPart){
          parameters[queryPart.split('=')[0]] = queryPart.split('=')[1] || null;
        });
      }
      var options = {
        ws: ws,
        message: data,
        method: segments[1],
        url: segments[2],
        query: query,
        parameters: parameters,
        headers: {},
      };
      if (process.env.USER) {
        options.user = {};
        options.user.name = process.env.USER;
      }

      helpers.findByUrl(options.url, options);
    });
  
  });

  helpers.log('**important** WebSockets are available on '+wsUrl+':'+config.websockets.port+'/');

}
