var config = require('../config');
var fs = require('fs');
var sys = require('sys')
var spawn = require('child_process').spawn;
var sudo = require('sudo');
var path = require('path');
var log = require('./logger');

var mongo = require('mongoskin');
var db = mongo.db(config.db.mongodb, { safe: false });
var routesCollection = db.collection('routes');

var helpers = {};

helpers.detectObjectValues = function detectObjectValues(o) {
  if ((typeof o === 'object') && (o !== null)) {
    for (var attr in o) {
      var value = o[attr];
      if ((typeof value === 'object')&&(value !== null)) {
        o[attr] = detectObjectValues(o[attr]);
      } else {
        if (typeof value === 'string')
          value = value.trim();
        if (/^([0-9]+[\.\,]{1}[0-9]*|[0-9]*[\.\,]{1}[0-9]+|[0-9]+)$/.test(value))
          o[attr] = Number(value.replace(',', '.'));
        else if (value === 'null')
          o[attr] = null;
        else if (value === 'true')
          o[attr] = true;
        else if (value === 'false')
          o[attr] = false;
        else if (value === 'NaN')
          o[attr] = NaN;
        else
          o[attr] = value;
      }
    }
  }
  return o;
}

helpers.outputToJSON = function(data) {
  if (data) {
    var keyValueFound = null;
    var object = {};
    data.split('\n').forEach(function(line){
      if (line.trim()) {        
        if (keyValueFound !== false) {
          var parts = line.split(/^([a-zA-Z\s0-9\_\-]+)\:[\s\t]*(.*)$/);
          keyValueFound = Boolean(parts && parts[1] && parts[2]);
          if (keyValueFound) {
            object[parts[1].trim()] = parts[2].trim();
          }
        }
      }
    });
    if (keyValueFound)
      return object;
  }
  return data;
}

helpers.relativeUrl = function(url) {
  return url.replace(new RegExp('^'+config.endpoint), '').replace(/\?.*$/,'');
}

helpers.absolutePath = function(file) {
  return file.replace(/^\/+/, __dirname+'/../routes/');
}

helpers.fileExists = function(file) {
  return fs.existsSync(helpers.absolutePath(file));
}

helpers.loadFile = function(file, method, options) {
  var routes = require('./'+file);
  if (typeof routes.all === 'function') {
    return routes.all(options, res);
  } else if (typeof routes[method] === 'function') {
    return routes[method](options);
  } else {
    return options.send("method '"+method+"' doesn't exists for file '"+file+"'", 404);
  }
}

helpers.executeFile = function(file, language, options) {
  var interpreter = config.allowedInterpreters[language];
  if (!interpreter)
    return options.send('Interpreter '+language+' is not supported/available', 500);
  var command = '-u '+options.user.name+' '+interpreter+' '+helpers.absolutePath(file) + '';
  
  var commands = [];

  if (options.body) {
    for (var attr in options.body) {
      // TODO: escape body[attr]
      var value = options.body[attr].replace(/^\"(.*)\"$/,'$1');
      command += ' '+attr + ' "' + value + '" ';
      commands.push('parameter_'+attr+'='+value.replace(/\s/g, '\\ '));
    }
  }
  if (options.params) {
    var enviromnmentsVariables = options.params;
    enviromnmentsVariables.TIMESTAMP = Math.floor(new Date().getTime());
    enviromnmentsVariables.BASE_URL = config.baseUrl(); 
    // { spawnOptions: { env: { NODE_ENV: 'test' } } } doesnt work here
    for (var variable in enviromnmentsVariables) {
      var value = enviromnmentsVariables[variable];
      commands.push(variable+'='+value);
    }
  }

  commands = commands.concat(command.split(/\s+/));

  var child = sudo(commands); // run as sudo
  var response = { error: null, stdout: '', stderr: '' };

  response.statusCode = 200;
  
  if ((options.headers['x-stream']) && (options.res)) {
    return child.stdout.pipe(options.res);
  }

  var lastHeartBeat = new Date().getTime(); // see setInterval() below
  var checkHeartbeatInterval = 0;

  child.stdout.on('data', function(data) {
    if (options.ws) {
      // websockets
      options.ws.send(String(data));
    } else {
      // else collect string for sending with response
      response.stdout += data;
      lastHeartBeat = new Date().getTime();
    }
  });

  child.stderr.on('data', function(stderr){
    response.statusCode = 500;
    if (/can(.+)\sopen/i.test(stderr))
      response.statusCode = 410
    else if (/permission/i.test(stderr))
      response.statusCode = 403
    else if (/command not found/i.test(stderr))
      response.statusCode = 503
    else if (/no .+ file .+ directory/i.test(stderr))
      response.statusCode = 404
    response.stderr += stderr;
    lastHeartBeat = new Date().getTime();
  });

  child.on('close', function(code) {
    var data = helpers.stringToObject(response.stdout);
    if (checkHeartbeatInterval)
      clearInterval(checkHeartbeatInterval);
    var responseData = {
      error: (response.stderr.trim()) ? response.stderr.trim() : null,
      data: data,
      status: response.statusCode,
    };
    if (response.error) {
      responseData.response = {
        error: response.error,
      };
    }
    if (config.attachDebugDataOnResponse) {
      responseData.std = {
        out: response.stdout.trim(),
        err: response.stderr.trim(),
      };
      responseData.process = {
        code: code,
        pid: child.pid,
      };
    }
    options.sendAsJson(responseData, response.statusCode);
  });

  if (!options.ws) {
    checkHeartbeatInterval = setInterval(function(){
      if ( (new Date().getTime() - lastHeartBeat) >= config.response.timeout ) {
        options.send('No signal in stdout since '+config.response.timeout+'[ms], killing process now', 504);
        helpers.log('**notice** Reached timeout for stdout, killing '+child.pid);
        process.kill(child.pid, 'SIGHUP'); 
        clearInterval(checkHeartbeatInterval);     
      }
    }, 5000);
  }
}

helpers.stringToObject = function(string) {
  try {
    // tabs arent allowed in json
    data = JSON.parse(string.replace(/\t/g,'  '));
  } catch(e) {
    data = helpers.outputToJSON(string);
    // helpers.log('DEBUG: JSON parse error', e.message);
    if ((typeof data === 'string') && (data)) {
      data = [];        
      string.split('\n').forEach(function(line){
        if (line.trim())
          data.push(line);
      });
      if (data.length === 0)
        data = [ string ];
    } 
  }
  if (typeof data === 'object')
    return helpers.detectObjectValues(data);
  else
    return null;
}

helpers.findByUrl = function(url, options) {
  var req = options.req;
  var res = options.res;
  if (req) {
    options.headers = req.headers;
  }
  options.send = function(data, status) {
    if (options.res)
      res.send(data, status);
    else if (status !== 200)
      helpers.log('**debug** Problem on response', data, status);
  }
  options.sendAsJson = function(data, status) {
    if (options.res)
      res.json(data, status);
  }
  url = helpers.relativeUrl(url);
  url = url.replace(/\/+$/,''); // remove trailing slash
  if (/^(\/[a-zA-Z\_\-0-9\.\*\+]+)*$/.test(url)) {
    var parts = url.match(/^(\/*.+?\/)(.*)$/);
    var regex = new RegExp(regexString);
    var query = {
      $or: [
        { uri: '*:'+url },
        { uri: options.method.toUpperCase()+':'+url },
      ]
    };

    if (parts) {
      var segments = [];
      if ((parts[1])&&(parts[2])) {
        var regexString = '^('+options.method.toUpperCase()+'|\\*)+\\:'+parts[1].replace(/\//g,'\\/')+'\\$[a-zA-Z\\_\\-]+';
        if (/\//.test(parts[2])) {
          regexString += '\\+';
          segments = parts[2].split('/');
        } else {
          regexString += '\\+*';
          segments = parts[2];
        }
        regexString += '$'
        query['$or'].push({ uri: new RegExp(regexString) });
      }
      if (typeof segments === 'string')
        segments = [ segments ];
    }

    routesCollection.findOne(query, function(err, found){
      if (err) {
        helpers.log('DEBUG: Problem matching route:', err.message);
        return options.send('Problem matching route: '+err, 404);
      } else if (!found) {
        return options.send('URL not found', 404);
      } else {
        helpers.log('DEBUG: found route', found.uri, found.file);
        var parameters = {};
        // [ 'GET:', 'log', '$file+' ]
        var catchAll = false;
        found.uri.split('/').forEach(function(uriPart){
          var key = uriPart.replace(/^\$(.+?)\+*$/,'$1');
          if (/^\$[a-zA-Z]+/.test(uriPart)) {
            if (catchAll) {
              parameters[catchAll] += '/'+segments.shift();
            } else if (/\+$/.test(uriPart)) {
              catchAll = key;
              parameters[catchAll] = segments.shift();
            } else {
              parameters[key] = segments.shift();
            }
          }
        });
        if ((segments)&&(segments.length > 0))
          parameters[catchAll] += '/'+segments.join('/');
        // /log/$file/$other -> { file: $file, other: $other }
        options.params = parameters;

        if (/\.js$/.test(found.file)) {
          // we have a js file
          // js files are used for direct routing processing
          return helpers.loadFile(found.file, options.method, options);
        } else {
          return helpers.executeFile(found.file, found.language, options);
        }
      }
    });
  } else {
    return options.res.send('Invalid url', 500);
  }
  
}

helpers.log = log;

module.exports = exports = helpers;