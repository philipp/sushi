var exports, log;

log = function() {
  var ansi, arg, args, color, colors, currentTime, defaultLevel, i, isError, s, time, useColors, _toS;
  currentTime = function() {
    return new Date().toString().replace(/^([a-z]+[\,\s]*?)([a-z].+)(GMT.*?)$/i, '$2').trim();
  };
  _toS = function(o) {
    if (typeof o === 'function') {
      return String(o.constructor);
    } else if (typeof o === 'object') {
      return JSON.stringify(o);
    } else {
      return String(o);
    }
  };
  args = Array.prototype.slice.call(arguments);
  colors = {
    '\u001b[0m': /.*/,
    '\u001b[31m': /err/i,
    '\u001b[36m': /info/i,
    '\u001b[1;36m': /(notice|debug)/i,
    // '\u001b[1;31m': /debug/i,
    '\u001b[33m': /important/i
  };
  useColors = true;
  defaultLevel = 'notice';
  isError = false;
  time = currentTime();
  s = (function() {
    var _i, _len, _results;
    _results = [];
    for (i = _i = 0, _len = args.length; _i < _len; i = ++_i) {
      arg = args[i];
      if (i === 0) {
        arg = String(arg);
        isError = /err/i.test(arg);
        color = '';
        if (/^\s*[\_\+\*\=\-]*(err|info|debug|notice|important)/i.test(arg)) {
          for (ansi in colors) {
            if (colors[ansi].test(arg) && useColors) {
              color = ansi;
            }
          }
          _results.push(time + ' ' + arg.replace(/^([\_\+\*\=\-]*([a-z]+)[\_\+\*\=\-]*)*\s*/i, "" + color + "[$2]\t") + ' ');
        } else {
          for (ansi in colors) {
            if (colors[ansi].test(defaultLevel) && useColors) {
              color = ansi;
            }
          }
          _results.push("" + time + " " + color + "[" + defaultLevel + "]\t" + arg + " ");
        }
      } else {
        _results.push(_toS(arg));
      }
    }
    return _results;
  })();
  return console[isError ? 'error' : 'log'](s.join(' ').trim() + Object.keys(colors)[0]);
};

module.exports = exports = log;
