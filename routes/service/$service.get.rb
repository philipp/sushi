status = `service #{ENV['service']} status`
puts 'status: ' + status
if /[0-9]+\s*$/.match(status)
  puts 'pid: ' + status.sub(/^.+?([0-9]+)\s*$/,'\1')
  puts "uri: #{ENV['BASE_URL']}process/" + status.sub(/^.+?([0-9]+)\s$/,'\1')
else
  puts 'pid: null'
  puts 'uri: null'
end