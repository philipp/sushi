services = `service --status-all 2>&1`#2>> /dev/null`

services = services.split(/\n/)

services = services.map {|line|
	match = line.match(/\s*\[\s*(.+?)\s*\]\s+(.+)$/)
	line = "{ \"#{match[2]}\": { \"status\": \"#{match[1]}\", \"uri\": \"#{ENV['BASE_URL']}service/#{match[2]}\" } }"
}

puts "[#{services.join(', ')}]"