process = `cat /proc/#{ENV['pid']}/status`

process = process.split(/\n+/).map { |line|
	parts = line.split(/\:\s/)
	"\"#{parts[0]}\": \"#{parts[1]}\""
}

puts '{'+process.join(', ')+'}'