var config = require('../config');
var helpers = require('../app/helpers');

exports.index = function(req, res){
	var options = {
		method: req.method.toLowerCase(),
		req: req,
		res: res,
	};
	
	if (process.env.USER) {
		options.user = {};
		options.user.name = process.env.USER;
	}

	// TODO: implement auth middleware
	if ((!options.user)||(!options.user.name)) {
		return res.send('You have to sign in as a valid user', 401);
	}

	return helpers.findByUrl(req.url, options);

};