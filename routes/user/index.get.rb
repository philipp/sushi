#!/usr/bin/env ruby -w

#ls /home | awk ' BEGIN { ORS = ""; print "["; } { print "\/\@"$0"\/\@"; } END { print "]"; }' | sed "s^\"^\\\\\"^g;s^\/\@\/\@^\", \"^g;s^\/\@^\"^g"
# require 'json'
# users = `ls /Users`
# puts users.split(/\n/).to_json

all = `cat /etc/passwd`

all = all.split(/\n/).map { |line|
  p = line.split(/\:/)
  line = <<eos
    { "#{p[0]}": {
        "name": "#{p[0]}",
        "group": "#{p[4]}",
        "id": "#{p[2]}",
        "group_id": "#{p[3]}",
        "home": "#{p[5]}",
        "uri": "#{ENV['BASE_URL']}user/#{p[0]}"
      }
    }
eos
}

puts "[#{all.join(', ')}]"#"[\"#{all.join('", "')}\"]"