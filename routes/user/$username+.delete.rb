#!/usr/bin/env ruby -w

`userdel #{ENV['username']} && rm -rf /home/#{ENV['username']}`