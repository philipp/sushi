var config = require('./config');
var helpers = require('./app/helpers')
var fs = require('fs');
var sys = require('sys')
var exec = require('child_process').exec;
var p = require('path');
var child;

var mongo = require('mongoskin');

var path = p.normalize(helpers.absolutePath('/')).replace(/\/+$/,'');

var buildIndex = function() {
  exec('find '+path, function(error, stdout, stderr) {
    var filesAbsolute = stdout.split('\n');
    var filesRelative = [];
    var files = {};
    var routes = {};
    filesAbsolute.forEach(function(file){
      var filename = file.replace(new RegExp('^'+__dirname+'/routes'), '');
      files[filename] = filename;
      filesRelative.push(filename);
    });
    filesRelative.forEach(function(filepath){
      var scriptfile = null;
      if ((filepath)&&(!/\/\./.test(filepath))) {
        var match = filepath.match(/^(.+?)(\.(post|get|put|delete))*(\.)([a-z]+)$/);
        // [ '/users/index.post.sh',
        //  '/users/index',
        //  '.post',
        //  'post',
        //  '.',
        //  'sh',
        //  index: 0,
        //  input: '/users/index.post.sh' ]
        if (match) {
          var url = ( (match[3]) ? match[3].toUpperCase() : '*' ) + ':' + match[1].replace(/\/index$/,'');
          scriptfile = __dirname + '/routes' + match[0] || null;
          routes[url] = {
            language: match[5] || null,
            file: match[0] || null,
            absolutePath: scriptfile,
          };
        } else {

          // '/services/$service': { method: '*', language: null, file: '/services' }
          var file = filepath.replace(/\/\$.+$/,'');
          // var interpreter = null;
          for (var language in config.allowedInterpreters) {
            if (files[file+'/index.'+language]) {
              file = file+'/index.'+language;
            }
          }
          // 
          scriptfile = __dirname + '/routes' + file;
          // check is folder
          // console.log(fs.fstatSync(scriptfile))
          routes['*:'+filepath.replace(/\+$/,'.*')] = {
            language: null,
            file: file,
            absolutePath: scriptfile,
          };
        }
      }
    });
    var count = Object.keys(routes).length;
    var todo = count;
    var added = 0;
    for (var uri in routes) {
      var data = routes[uri];
      data.uri = uri;
      // only add files with (allowed) script extension
      if (! new RegExp('\.('+Object.keys(config.allowedInterpreters).join('|')+')$').test(data.file) ) {
        todo--;
        continue;
      }
      collection.insert(data, function() {
        todo--;
        added++;
        if (todo === 0) {
          console.log('Inserted '+added+' routes');
          process.exit(0);
        }
      });
      
      // 
    }
  });
}


var db = mongo.db(config.db.mongodb, { safe: false })
var collection = db.collection('routes');

collection.drop(function() {
  db.createCollection('routes', function() {
    collection = db.collection('routes');
    collection.ensureIndex([['uri', 1]], true, buildIndex);
  })
  
  // buildIndex
});
